from conans import ConanFile, tools, CMake

class TaglibConan(ConanFile):
    name = "taglib"
    version = "git"
    license = "https://github.com/taglib/taglib/blob/master/COPYING.LGPL"
    url = "https://github.com/taglib/taglib"
    no_copy_source = True

    def source(self):
        self.run("git clone https://github.com/taglib/taglib.git")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="taglib")
        cmake.build()
        cmake.install()

    def package_info(self):
        self.libs = ['taglib']
